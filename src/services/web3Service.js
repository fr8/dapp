import Web3 from 'web3'
import BN from "bn.js"

const web3Service = {
  web3: null,
  accounts: [],
  network: 'Unknown',
  networkId: 0,
  init: function () {
    if (!window.web3) {
      return;
    }
    this.web3 = new Web3(window.web3.currentProvider)
    this.accounts = []
    this.checkCurrentNetwork()
    // this.openMetamaskPopup()
  },
  getAccounts: function () {
    return new Promise((resolve, reject) => {
      this.web3.eth.getAccounts((error, accounts) => {
        if (error) {
          reject(error)
        }
        this.accounts = accounts
        resolve(accounts[0] || null)
      })
    })
  },
  checkCurrentNetwork: function () {
    window.web3.version.getNetwork((error, network) => {
      if (!error) {
        this.networkId = network;
        switch (network) {
          case '1':
            this.network = 'Main Network'
            break
          case '3':
            this.network = 'Ropsten Test Network'
            break
          case '4':
            this.network = 'Rinkeby Test Network'
            break
          case '42':
            this.network = 'Kovan Test Network'
            break
          default:
            this.network = 'Unknown'
            break
        }
      }
    })
  },
  getBalance: function (address) {
    return new Promise((resolve, reject) => {
      this.web3.eth.getBalance(address, (error, resp) => {
        if (error) {
          reject(error)
        }
        resolve(resp)
      })
    })
  },
  getTokenBalance: function (ownerAddress, tknAddress) {
    return new Promise((resolve, reject) => {
      let txOptions = {
        to: tknAddress,
        data: this.web3.eth.abi.encodeFunctionCall({
            name: 'balanceOf',
            type: 'function',
            inputs: [{
                type: 'address',
                name: 'address'
            }]
        }, [ownerAddress])
      }
      let token_decimals = process.env.VUE_APP_ARY_TOKEN_DECIMALS
      this.web3.eth.call(txOptions).then(resp => {
        let balanceInWei = this.web3.utils.toBN(resp)
        // token_decimals = token_decimals / 2

        // // Cannot create a BigNumber greater than 2^53 (~10^16).
        // let divider = new BN(Math.pow(10, token_decimals))
        // balanceInWei = balanceInWei.div(divider)
        // balanceInWei = balanceInWei.div(divider).toString()

        // let balanceInEdi = parseFloat(balanceInWei)
        resolve(parseFloat(balanceInWei))
      })
      .catch(reject)
    })
  },
  getEstimatedGas: function (byteCode) {
    return new Promise((resolve, reject) => {
      this.web3.eth.estimateGas({data: byteCode}, (error, resp) => {
        if (error) {
          reject(error)
        }
        resolve(resp)
      })
    })
  },
  getGasPrice: function () {
    return new Promise((resolve, reject) => {
      this.web3.eth.getGasPrice((error, resp) => {
        if (error) {
          reject(error)
        }
        resolve(resp)
      })
    })
  },
  getTransactionsCount: function (address) {
    return new Promise((resolve, reject) => {
      this.web3.eth.getTransactionCount(address, (error, resp) => {
        if (error) {
          reject(error)
        }
        resolve(resp)
      })
    })
  },
  getTransactionReceipt: function (txHash) {
    return new Promise((resolve, reject) => {
      this.web3.eth.getTransactionReceipt(txHash, (error, resp) => {
        if (error) {
          reject(error)
        }
        resolve(resp)
      })
    })
  },
  sendTransaction: function (transaction) {
    return new Promise((resolve, reject) => {
      this.openMetamaskPopup()
      this.web3.eth.sendTransaction(transaction, (error, resp) => {
        if (error) {
          reject(error)
        }
        this.closeMetamaskPopup()
        resolve(resp)
      })
    })
  },
  getContractInstance: function (contract, from) {
    let instance = new this.web3.eth.Contract(JSON.parse(contract.abi), contract.address, {'from': from})
    return instance
  },
  getContractMethods: function (abi) {
    abi = JSON.parse(abi)
    let methods = abi.filter(item => { return item.type === 'function' })
    return methods
  },
  deployContract: function (from, contractData) {
    return new Promise((resolve, reject) => {
      var contractInstance = window.web3.eth.contract(JSON.parse(contractData.abi))
      contractInstance.new({
        from: from,
        data: contractData.byte_code,
        gas: contractData.gas_limit
      }, (error, resp) => {
        if (error) {
          reject(error)
        }
        resolve(resp)
      })
    })
  },
  callContractMethod: function (contractData, method, fromAddr) {
    let contract = new this.web3.eth.Contract(JSON.parse(contractData.abi), contractData.address)

    return new Promise((resolve, reject) => {
      let params = method.inputs.map(value => value.type === 'unint256' ? parseInt(value.value) : value.value)
      contract.methods[method.name](...params).call({from: fromAddr}, (error, result) => {
        if (error) {
          reject(error)
        }
        resolve(result)
      })
    })
  },
  sendEther: function (contractFunction) {
    this.web3.eth.sendTransaction({
      to: '0x8f6c0c887F7CAF7D512C964eA2a3e668D94C5304',
      value: '1000000000000'
    }, (err, res) => {
      if (err) this.closeMetamaskNotification()
      if (res) this.closeMetamaskNotification()
    })

    setTimeout(() => {
      this.openMetamaskNotification()
    }, 500)
  },
  watchMetamaskAddressUpdate: function (updateCallback) {
    this.web3.currentProvider.publicConfigStore.on('update', (update) => {
      const currentAddress = this.accounts[0] ? this.accounts[0].toLowerCase() : '';
      if (currentAddress && update.selectedAddress !== currentAddress) {
        this.accounts[0] = update.selectedAddress;
        updateCallback(update);
      }
    });
  }
}

web3Service.init();
export default web3Service
