import web3Service from '@/services/web3Service'
import token_abi from '@/contracts/token'

const tokenService = {
  web3: null,
  contract: null,
  members_allowed: 0,

  init: function () {
    this.web3 = web3Service.web3
    this.contract = new this.web3.eth.Contract(token_abi, process.env.VUE_APP_TOKEN_CONTRACT)

    this.getMembersAllowed()
    this.getWhitelistedMembers()
  },

  getMembersAllowed: function() {
    return new Promise((resolve, reject) => {
      let txOptions = {
        to: process.env.VUE_APP_TOKEN_CONTRACT,
        data: this.web3.eth.abi.encodeFunctionCall({
          name: 'getAllowedWhitelistedAddresses',
          type: 'function',
          inputs: []
        }, [])
      }
      this.web3.eth.call(txOptions).then(resp => {
        this.members_allowed = this.web3.utils.toBN(resp).toString()
        resolve(this.members_allowed)
      })
      .catch(reject)
    })
  },

  getWhitelistedMembers: function (license_id = 0) {
    return new Promise((resolve, reject) => {
      this.contract.methods.getLicenseWhitelistedAddresses(license_id)
      .call((error, result) => {
        if (error) {
          reject(error)
        }
        resolve(result)
      })
    })
  },
  fetchlicenseId: function (companyId) {
    return new Promise((resolve, reject)=> {
      let owner = web3Service.accounts[0]
      this.contract.methods.getLicenseIdByCompanyId(companyId).call({ from: owner }).then((resp) => {
        if (!resp) {
          reject(resp)
        }
        resolve(resp)
      }).catch(error => {
        console.log(error)
        reject(error)
      })
    })
  },
  fetchLicenseInfo: function (licenseId) {
    return new Promise((resolve, reject)=> {
      let owner = web3Service.accounts[0]
      this.contract.methods.getLicenseInfo(licenseId).call({ from: owner }).then((resp) => {
        if (!resp) {
          reject(resp)
        }
        resolve(resp)
      }).catch(error => {
        console.log(error)
      })
    })
  },
  checkCompanyOwner: function (licenseId) {
    return new Promise((resolve, reject) => {
      let address = web3Service.accounts[0]
      this.contract.methods.ownerOf(licenseId).call({from: address}).then((ownerAddress) => {
        let isOwner = (ownerAddress == address)
        resolve(isOwner)
      }).catch((error) => {
        console.log(error)
        reject(false)
      })
    })
  },
  verifyLicense: function (licenseId) {
    return new Promise((resolve, reject)=> {
      let owner = web3Service.accounts[0]
      this.contract.methods.isValidLicense(licenseId).call({ from: owner }).then((resp) => {
        if (!resp) {
          reject(resp)
        }
        resolve(resp)
      }).catch(error => {
        console.log(error)
      })
    })
  },
  verifyCompanyLicense: function(companyId){
    return new Promise((resolve, reject) => {
      this.fetchlicenseId(companyId).then(licenseId => {
        this.verifyLicense(licenseId).then(license => {
          if (!license) {
            reject(license)
          }
          resolve(license)
        })
      }, error => {
        reject(error)
      })
    })
  }
}

export default tokenService;