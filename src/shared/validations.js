export default {
  name: [
    {
      required: false,
      message: 'Name is required',
      trigger: 'blur'
    }
  ],
  email: [
    {
      required: false,
      message: 'Email is required',
      trigger: 'blur'
    },
    {
      pattern: /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/,
      message: 'Invalid email address',
      trigger: 'blur'
    }
  ],
  password: [
    {
      required: false,
      message: 'Password is required',
      trigger: 'blur'
    },
    {
      min: 6,
      message: 'Password should be atleast 6 characters in length',
      trigger: 'blur'
    },
    {
      pattern: /^[^\s]+$/,
      message: 'Password may not contain spaces.',
      trigger: 'blur'
    }
  ],
  website: [
    {
      required: true,
      message: 'Website is required',
      trigger: 'blur'
    }, 
    {
      pattern: /(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/,
      message: 'Invalid website url',
      trigger: 'blur'
    }
  ],
  phone: [
    {
      required: false,
      message: 'Phone no. is required',
      trigger: 'blur'
    }
  ]
}